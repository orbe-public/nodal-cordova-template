Nodal Cordova template
======================

The **Nodal Cordova template** allow you to use preexisting code to jump start your [Nodal.Studio project](https://dev2.nodal.studio/) to Android and iOS devices.



Create a new project using this template
-------------------------------------------

Go to the directory where you want to save your sources codes and create a new Nodal.Studio Cordova project by executing the following command `cordova create path id name --template git+https://gitlab.com/orbe-public/nodal-cordova-template` and by fill in your own options beforehand.

- *path*, the directory to save everything (which should not already exist, Cordova will create this directory).
- *id*, a reverse domain-style identifier that must be unique, it is recommended that you select an appropriate value.
- *name*, the application's display name.


For example:
```bash
cordova create helloworld mobi.orbe.helloworld "Hello World" --template git+https://gitlab.com/orbe-public/nodal-cordova-template
```
This creates the required directory structure for your Nodal.Studio Cordova app. By default, the `cordova create` script generates a skeletal web-based application whose home page is the project's `www/index.html` file and the configuration file `www/js/config.js`. See the [Cordova create command reference documentation](http://cordova.apache.org/docs/en/latest/reference/cordova-cli/index.html#cordova-create-command) for complete explanations.





Configure your project 
----------------------

Open the Nodal.Studio Cordova configuration file `www/js/config.js`, read instructions and modifies his content to match your app requirements.

All subsequent commands need to be run within the project's directory.  
1. go to this directory:
     ```bash
     cd hello
     ```
2. select the optimized build version if needed (i.e. the essential plugins) replacing the default `package.json` with the `package.opt.json` file: 
     ```bash
     mv -f package.opt.json package.json
     ```
3. (optional) customize your Cordova build (platforms, plugins, etc.) by editing the `package.json` file.
4. prepare your project for building for Android and iOS:
     ```bash
     cordova prepare
     ```
5. (optional) to build and run apps, you need to install SDKs for each platform. Check and install pre-requisites for building by running the command below:
     ```bash
     cordova requirements
     ```


### iOS requirements
Signing your app requires a development team and some capabilities enable. 
1. open the Xcode project `platforms/ios/__APPNAME__.xcodeproj`
2. select the app target in the project editor
2. open the general pane
3. select a development team in the signing section
4. open the capabilities pane
5. turn on (if needed) the NFC tag reading
6. turn on (if needed) the backgrounds modes and enable *Audio, Airplay*, *Location updates* and *Uses Bluethooth LE accessories* modes

Notifications messages could be personalized in the info pane in iOS target properties section or directly in the `platforms/ios/__APPNAME__/__APPNAME__-Info.plist`





Setup a build with local assets 
-------------------------------

If you need to create an app with local assets built-in the app you should download all media & dataproject of your project with the helper script:
1. go to your experience's URL, e.g. `https://dev2.nodal.mobi/orbe/welcome/to` (with no screen specified). This step is needed to refresh the dataproject JSON to the lastest version.
2. download the .zip archive of your project by adding `authoring/downloadproject/` before your experience's URL, e.g. `https://dev2.nodal.mobi/authoring/downloadproject/orbe/welcome/to`
3. decompress the archive and move or copy/paste the `media` folder into your */path/to/your/cordova/project/www* directory, the `dataproject.json`, the `player.json` and the `player.js` files into the */path/to/your/cordova/project/www/js* directory
4. set the property `localAssets` to true in the *www/js/config.js* configuration file



Build and run your project 
--------------------------

Connect your device, compile, install and run your project on the specified platform by running one of these commands:
```bash
cordova run android
cordova run ios
```





About this template
-------------------

Begin by creating a Nodal.Studio Cordova app that will become the basis for this template. Then take the contents of your app and put them into the following structure. When your template is used, all of the contents within `template_src` will be used to create the new project, so be sure to include any necessary files in that folder. Reference [this example](https://github.com/carynbear/cordova-template) for details.

```
template_package/
├── index.js        (required)
├── package.json    (required)
└── template_src/   (required)
	└── CONTENTS OF APP TEMPLATE
```
Note that `index.js` should export a reference to `template_src` and `package.json` should reference `index.js`. See [the example](https://github.com/carynbear/cordova-template) for details on how that is done.