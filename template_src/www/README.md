How to personalize and optimize your Nodal.Studio app
=====================================================


Update
------

This template certainly includes some old files compared to the development of the new features of the Nodal.Studio project. You can update the "core" of this template by replacing or adding files listed below:

- update the client part: `js/player.js` and `js/player.json` files
- update the authoring dependencies: `authoring/*json` files
- update the defaut app styles: `css/main.css` file



Plugins
-------

Some plugins are pre-installed (but not necessarily needed, see the plugins section below), some other or their preferences could increase your app functionalities. Read this page for a complete documentation: https://orbe.atlassian.net/wiki/spaces/NS/pages/425197583/Cordova#Plugins

To list currently installed plugins, enter the command line below in your Terminal.
```bash
cd /path/to/the/cordova/project/root/folder 
cordova plugin ls
```


If you want to customize the plugin *com.unarin.cordova.beacon* Android permission, open the `platforms/android/app/src/main/java/com/unarin/cordova/beacon/LocationManager.java` file or toggle the `app > java > com > unarin.cordova.beacon > LocationManager` tree in Android studio and modifies the title and the message arguments at line 314 and 315 to your choice, see below.

```java
builder.setTitle("This app needs location access"); // line 314
builder.setMessage("Please grant location access so this app can detect beacons."); // line 315
```





Configure the policy to fit your own app's needs
------------------------------------------------

Add, remove or update the meta *http-equiv="Content-Security-Policy"* content to controls which network requests (images, XHRs, etc) are allowed to be made via the webview directly.
For more guidance, see: https://github.com/apache/cordova-plugin-whitelist/blob/master/README.md#content-security-policy  
Some notes:
* gap: is required only on iOS (when using UIWebView) and is needed for JS->native communication
* https://ssl.gstatic.com is required only on Android and is needed for TalkBack to function properly
* disables use of inline scripts in order to mitigate risk of XSS vulnerabilities. To change this:
    * enable inline JS: add 'unsafe-inline' to default-src



Optimize the app size
---------------------

To free space and reduce the app size, you can safely remove all fonts except the ones use in your project. To do so, open the www/fonts directory and trash all unneeded files.

Not all plugins are required to properly run your project. For some validation constraints (IOS in particular), fluidity and size of the final app, it is advisable to remove non-useful plugins. e.g., if your project does not use OSC, the *cordova-plugin-osc* plugin becomes inessential. To remove a plugin and all his functionality enter the following command line in your Terminal and rebuild your app.

```bash
cd /path/to/the/cordova/project/root/folder 
cordova plugin rm PLUGIN_NAME
```