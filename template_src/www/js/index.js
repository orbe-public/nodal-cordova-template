/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
const app = {
    /**
     * Application constructor.
     */
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    /**
     * deviceready Event Handler.
     * Bind any cordova events here. Common events are: 'pause', 'resume', etc.
     */
    onDeviceReady: function() {

        // detect platform, add additional class for CSS and store data for Nodal.Studio requirements
        // plugin: cordova-plugin-device (required)
        // https://cordova.apache.org/docs/en/9.x/reference/cordova-plugin-device/index.html#deviceplatform
        document.body.className += device.platform === 'iOS' ? "platform-ios" : "platform-android";
        cordova.device = device;
        Logger.info('Device Model: ', device.model);
        Logger.info('Device Cordova: ', device.cordova);
        Logger.info('Device Platform: ', device.platform);
        Logger.info('Device UUID: ', device.uuid);
        Logger.info('Device Version: ', device.version);



        // register events
        // https://cordova.apache.org/docs/en/9.x/cordova/events/events.html
        // Events fired when the native platform puts/pulls the app into/out from the background.
        document.addEventListener('pause', this.onPause.bind(this), false);
        document.addEventListener('resume', this.onResume.bind(this), false);


        // Defines a drop-in replacement for the window.open() function.
        // plugin: cordova-plugin-inappbrowser
        // https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-inappbrowser/
        if( cordova.InAppBrowser ) {
            window.open = cordova.InAppBrowser.open;
        }

        // plugin: cordova-plugin-statusbar
        // https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-statusbar/
        if (window.StatusBar) {
            window.StatusBar.hide();
        }

        // Perform infinite background execution (iOS<13 or Android)
        // plugin: cordova-plugin-background-mode
        // https://github.com/katzer/cordova-plugin-background-mode
        if (cordova.plugins && cordova.plugins.backgroundMode && (device.platform === 'Android' || parseInt(device.version) < 13)) {

            cordova.plugins.backgroundMode.setEnabled(true);
            Logger.info('Background mode is activated ?', cordova.plugins.backgroundMode.isActive() ? 'true' : 'false' ); 

            cordova.plugins.backgroundMode.on('activate', () => {
                Logger.info('Enter background mode');
                cordova.plugins.backgroundMode.disableWebViewOptimizations();
            });
            cordova.plugins.backgroundMode.on('deactivate', () => {
                Logger.info('Exit background mode');
            });
            cordova.plugins.backgroundMode.on('failure', () => {
                Logger.info('Background mode failed');
            });

            // (android only)
            if (device.platform === 'Android') {
                cordova.plugins.backgroundMode.isScreenOff(bool => {
                    Logger.info('Screen is off', bool);
                });
            }
        }

        // Adjust audio output volume
        // plugin: cordova-plugin-systemvolume + cordova-plugin-headsetdetection
        // https://github.com/happinov/cordova-plugin-systemvolume
        // https://github.com/EddyVerbruggen/HeadsetDetection-PhoneGap-Plugin
        if (window.SystemVolume) {
            // a float between 0 and 100 (65 for the common hearing limit level)
            let volume = 65;

            window.HeadsetDetection.registerRemoteEvents( status => {
                switch (status) {
                    case 'headsetAdded':
                        Logger.info('Headset was added');
                        break;
                    case 'headsetRemoved':
                        Logger.info('Headset was removed');
                        break;
                };
                Logger.info('Set system volume to', volume);
                window.SystemVolume.set(volume);
            });

            Logger.info('Set system volume to', volume);
            window.SystemVolume.set(volume);
        }

        // stay awake
        // plugin: cordova-plugin-insomnia
        // https://github.com/EddyVerbruggen/Insomnia-PhoneGap-Plugin
        if (window.plugins.insomnia) {
            window.plugins.insomnia.keepAwake();
        }

        // handle memory warning
        // plugin: cordova-plugin-memory-warning
        // https://github.com/88dots/cordova-plugin-memory-warning
        if (cordova.plugins && cordova.plugins.CordovaPluginMemoryWarning) {
            // release memory here
            const memoryHandler = () => {
                alert('Memory warning, free resources or quit/reload the app as soon as possible.');
            }
            document.addEventListener('memorywarning', memoryHandler);

            // because android does not have app specific memory warnings
            // you should manually check memory state before performing memory intensive operations
            // executes a callback that resolves with boolean true when memory usage is at an unsafe level
            // optionally pass a second callback for error handling
            if (device.platform === 'Android') {
                cordova.plugins.CordovaPluginMemoryWarning.isMemoryUsageUnsafe(
                    (result) => {
                        if (result) memoryHandler();
                    },
                    (error) => console.warn('memory warnings:', error)
                );
            }
        }

        // then configure Nodal.Studio app and (auto) start the project.
       this.configure();
    },

    /**
     * pause Event Handler.
     * The pause event fires when the native platform puts the application into the background.
     */
    onPause: function() {
        Logger.info('Enter background');
        // do something here…
    },

    /**
     * resume Event Handler.
     * The resume event fires when the native platform pulls the application out from the background.
     */
    onResume: function () {
        Logger.info('Exit background');
        // do something here…
    },

    /**
     * Check and extend the user configuration (define access, the require player, etc…).
     */
    configure: function() {
        if (!window.nodalConfig) {
            alert('The app configuration is missing or have errors. Please report this message to the administrator.');
            return;
        }

        const config   = window.nodalConfig || {};
        config.offline = config.offlineMode || false;
        config.local   = config.offline ? true : (config.localAssets || false);
        config.url     = new URL(config.experienceURL);
        config.server  = config.url.protocol + '//' + config.url.hostname;
        config.params  = config.url.pathname.slice(1).split(/\/+/);
        config.slug    = config.params[1];
        config.player  = config.offline || config.playerPath ? (config.playerPath || '') : `${config.server}/js/player.js`;
        config.project = config.offline || config.dataprojectPath ? (config.dataprojectPath || '') : `${config.server}/exp-config/${config.params.join('/')}`;

        Logger.log(
            'Nodal.Studio config:\n',
            `\texperience URL ..... ${config.experienceURL}\n`,
            `\tconfig params ...... ${config.params}\n`,
            `\toffline mode ....... ${config.offlineMode}\n`,
            `\tlocal assets ....... ${config.localAssets}\n`,
            `\tplayer path ........ ${config.player}\n`,
            `\tdataproject path ... ${config.project}\n`,
            `\tlog level .......... ${config.logLevel}\n\n`,
        );


        // verify the target URL (if needed) and check if the player exists
        // then init the app with this configuration
        const beforeinit = () => {
            const checkPlayer = () => {
                fileExists(config.player).then(
                    success => this.init(config),
                    error => alert(error.message)
                );
            };
            // the project URL must be valid for the websocket 
            // (or to download ressources)
            if (!config.offline) {
                fileExists(config.url).then(
                    success => checkPlayer(),
                    error => alert(error.message)
                );
            }
            else {
                if (!config.player) {
                    alert(`The player path is not properly set. Please report this message to the administrator.`);
                    return;
                };
                checkPlayer();
            }
        };

        // check if the data project exists and properly configured
        // or/then on success init the app on success
        if (config.offline) {
            if (!config.project) {
                alert(`The data project path is not properly set. Please report this message to the administrator.`);
                return;
            };
            fileExists(config.project).then(
                success => beforeinit(),
                error => alert(error.message)
            );
        }
        else beforeinit();
    },

    /**
     * Initilize and start the Nodal.Studio application.
     * @param {Object} config - the Nodal.Studio configuration
     */
    init: function(config) {
        let info = {};
        const domain = config.url.hostname;
        const subdomain = domain.split('.')[0];

        // fetch player information
        // @todo: no longer required when configuration is synchronized, see below
        fetchFile(`${config.player}on`).then(response => {
            if (response.status == 200) return response.json();
            else alert(`Failed to load '${config.player}on' [${response.status}]`);
        })
        .then(data => info = data)
        .then(() => {

            // define required configuration
            window.cordovaConfig = {
                offline: config.offline,
                local: config.local,
                urlServer: config.server,
                urlParams: config.params,
                dataproject: config.project,
                player: config.player,
            }

            // @todo: fetch the window.soundworksConfig from the server [ticket NS-369]
            // and assign the includeCordovaTags to true, the logLevel to this config.logLevel value
            window.soundworksConfig = {
                appName: config.slug,
                assetsDomain: config.domains ? config.domains.assets : 'assets.nodal.studio',
                bdd: {
                    url: 'https://' + (config.domains ? config.domains.database : `db.${subdomain}.nodal.studio`),
                    dbName: 'cosima',
                    designView: 'slug/',
                    settingsView: 'settings/'
                },
                clientType: 'player',
                defaultType: 'player',
                env: 'production',
                includeCordovaTags: true,
                localAssetDomain: domain,
                serverPort: 8000, // <----------------- set by the proxymanager should be synced !!!
                serverUrl: `//${domain}`,
                version: info && info.version.name || '0.0.0',
                websockets: {
                    url: `https://${domain}`,
                    transports: ['websocket'],
                    path: `/socket.io/${config.slug}`, // <-------- assume slug is used by the proxy
                    initialPacket: ['0']
                },
                logLevel: config.logLevel || 0
            }

            // append player
            const js = document.createElement("script");

            js.language = "javascript";
            js.type = "text/javascript";
            js.src = config.player;
            js.onload = () => { window.dispatchEvent(new Event('cordovaLoading')); };

            document.getElementsByTagName('body')[0].appendChild(js);
        })
        .catch(error => alert(error.message))
    },
};

/**
 * Checks whether a file exist locally or remotely.
 * 
 * @param {String} url - The file URL to be checked.
 * @return {Promise} Promise that will resolve if the file exists, and reject otherwise.
 */
function fileExists(url) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest;

        xhr.onload = () => {
            const status = xhr.status;
            if (status === 0 || (status >= 200 && status < 400)) resolve(true);
            else reject(new Error(
                `The requested URL\n"${url}"\nis missing or invalid: ${status} ${xhr.statusText.toLowerCase()}.Please report this message to the administrator.`
            ));
        }
        xhr.onerror = () => reject(new Error(
            `Failed to load the requested resource\n"${url}"\nPlease report this message to the administrator.`
        ));

        xhr.open('HEAD', url);
        xhr.send();
    });
}

/**
 * Fetches a file from a URL. Compatible with both http:// and file:// protocols,
 * which is not the case with the Fetch API on Android.
 * @see https://github.com/github/fetch/pull/92
 *
 * @param  {String} url
 * @return {Promise} Promise that will resolve if the file was loaded,
 *  and reject otherwise.
 */
function fetchFile(url) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest;

        xhr.onload = () => resolve(new Response(xhr.responseText, { status: xhr.status || 200 }));
        xhr.onerror = () => reject(new Error(
            `Could not fetch the requested file\n"${url}"\nPlease report this message to the administrator.`
        ));

        xhr.open('GET', url);
        xhr.send();
    });
}

/**
 * Extend the QR Code interface with custom elements.
 * 
 * @param {Object} interface - the QR Code interface (i.e the main HTMLDivElement)
 */
function QRCodeScannerCallback(interface) {
    // // define your own elements here, for ex.
    // var button = document.createElement('div');
    // button.setAttribute('id', 'qr-scanner-bt-cancel');
    // button.onclick = () => { alert('cancel'); }
    // interface.appendChild(button);
}

/**
 * If you need to change the target in which to load the URL, modifies the second argument in the 
 * cordova.InAppBrowser.open() method below with:
 * 
 *    - '_self', opens in the Cordova WebView if the URL is in the white list, 
 *               otherwise it opens in the `InAppBrowser`
 *    - '_blank', opens in the `InAppBrowser`
 *    - '_system', opens in the system's web browser
 * 
 * More information @ https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-inappbrowser/
 *
 * @param {String} link - the URL to load
 */
function openLink(link) {
    cordova.InAppBrowser.open(link, '_system', 'location=no');
}

/**
 * Send data to the console depending on the log level.
 * @todo should be override or linked to the nodal-app logger [ticket NS-113]
 */
Logger = {
    log: function(...args) {
        if (Logger.level >= 3) console.log(...args);
    },

    info: function(...args) {
        if (Logger.level >= 4) console.log(...args);
    },

    level: window.soundworksConfig ? window.soundworksConfig.logLevel :
                                     window.nodalConfig && window.nodalConfig.logLevel,
}

app.initialize();
