/**
 * This configuration file define the project properties.
 * 
 * THIS OBJECT WILL BE AUTOMATICALLY OVERWRITTEN BY THE APP SCRIPTS. If for any reasons you shoud 
 * change some application settings, have a look to the init() method in the 'index.js' file.
 */
window.nodalConfig = { 
 
    // The project URL
    experienceURL: "https://dev2.nodal.mobi/orbe/welcome/to/cordova",

    // Work online or completely offline.
    // Sets this property to true if you not need to sync or exchange datas with the server.
    // In that case all your project assets should be stored locally (dataproject, medias, player).
    offlineMode: false,

    // Whether media is stored locally or needs to be fetched online.
    // If the 'offlineMode' property is set to true, this property will be omitted.
    localAssets: false,

    // The dataproject relative or URL.
    // Leave blank or comment the line below to retrieve the project from the remote server.
    //dataprojectPath: "https://dev2.nodal.mobi/exp-config/orbe/welcome/to/cordova",
    //dataprojectPath: "js/dataproject.json",

    // The JavaScript player relative path or URL.
    // Leave blank or comment the line below to retrieve the player from the remote server.
    //playerPath: "js/player.js",

    // Define the log level:
    //  0 = quiet (default), should be used for production to increase speed
    //  1 = error (only)
    //  2 = warning
    //  3 = log
    //  4 = info
    //  5 = debug, full verbose mode
    logLevel: 0,



    // --- ADVANCED PROPERTIES ---------------------------------------------------------------------

    // All domain names below must match your network settings.
    // Uncomment these lines only to set custom domains if needed
    // @todo: to remove while the configuration is synced with the server [ticket NS-369]
    /*domains: {
        assets: 'assets.nodal.studio',
        database: 'db.dev2.nodal.mobi',
    }*/
}
