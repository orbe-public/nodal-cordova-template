const fs = require('fs');
const path = require('path');

module.exports = (context) => {
    if (context.opts.cordova.platforms.includes('android') && context.hook === 'after_plugin_install') {
        const plugin = context.opts.plugin.id;

        // update android.json
        const updateCoreConfig = (settings) => {
            const json = path.join(context.opts.projectRoot, 'platforms/android/android.json');
            fs.readFile(json, 'utf-8', function (err, data) {
                if (err) console.warn('Patch android config failed:', err.message);
                else {
                    data = JSON.parse(data);
                    const configs = data['config_munge']['files']['AndroidManifest.xml']['parents']['/manifest'];
                    settings.forEach(value => configs.splice(configs.findIndex(v => v.xml === value), 1));
                    fs.writeFile(json, JSON.stringify(data), 'utf-8', function (err) {
                        err || console.log('Patch Android config successfully');
                    });
                }
            });
        }

        // Manifest manage duplicate settings
        if (plugin === 'cordova-plugin-qrscanner-plus' || plugin === 'com.virtuoworks.cordova-plugin-canvascamera') {
            const manifest = path.join(context.opts.projectRoot, 'platforms/android/app/src/main/AndroidManifest.xml');
            fs.readFile(manifest, 'utf-8', function (err, data) {
                if (!err) {
                    if ((data.match(/android.permission.CAMERA/g)).length > 1) {
                        const settings = ['<uses-feature android:name="android.hardware.camera" />', '<uses-permission android:name="android.permission.CAMERA" />'];
                        settings.forEach(value => data = data.replace(value, ''));
                        fs.writeFile(manifest, data, 'utf-8', function (err) {
                            err || console.log('Patch Android manifest successfully');
                            updateCoreConfig(settings);
                        });
                    }
                }
            });
        }

        // fix cordova-plugin-qrscanner issue
        if (plugin === 'cordova-plugin-qrscanner-plus') {
            const qrscanner = path.join(context.opts.projectRoot, 'platforms/android/app/src/main/java/com/bitpay/cordova/qrscanner/QRScanner.java');
            fs.readFile(qrscanner, 'utf-8', function (err, data) {
                if (err) console.warn('Patch plugin "cordova-plugin-qrscanner-plus" failed:', err.message);
                else {
                    data = data.replace('import android.support.v4.app.ActivityCompat', 'import androidx.core.app.ActivityCompat');
                    fs.writeFile(qrscanner, data, 'utf-8', function (err) {
                        err || console.log('Patch plugin "cordova-plugin-qrscanner-plus" successfully');
                    });
                }
            });
        }
    }
}