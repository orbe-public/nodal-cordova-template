const fs = require('fs');
const path = require('path');

module.exports = (context) => {
    if (context.opts.cordova.platforms.includes('android')) {

        // update Gradle file
        const gradle = path.join(context.opts.projectRoot, 'platforms/android/app/build.gradle');
        fs.readFile(gradle, 'utf-8', function (err, data) {
            if (err) console.warn('Patch Gradle file failed:', err.message);
            else {
                // data = data.replace('android {\n    defaultConfig {', 'android {\n    defaultConfig {\n        // change the default generated apk/bundle name\n        versionName privateHelpers.extractStringFromManifest("versionName")\n        setProperty("archivesBaseName", "$applicationId-$versionName")\n');
                data = data.replace('applicationId privateHelpers.extractStringFromManifest("package")', 'applicationId privateHelpers.extractStringFromManifest("package")\n\n        // change the default generated apk/bundle name\n        versionName privateHelpers.extractStringFromManifest("versionName")\n        setProperty("archivesBaseName", "$applicationId-$versionName")');
                fs.writeFile(gradle, data, 'utf-8', function (err) {
                    err || console.log('Patch Gradle file successfully');
                });
            }
        });
    }
}